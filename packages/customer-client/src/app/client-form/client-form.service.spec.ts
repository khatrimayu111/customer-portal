import { TestBed } from '@angular/core/testing';

import { ClientFormService } from './client-form.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ClientFormService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    }),
  );

  it('should be created', () => {
    const service: ClientFormService = TestBed.get(ClientFormService);
    expect(service).toBeTruthy();
  });
});
