import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { GetCustomerEnquiryQuery } from './customer-enquiry.query';
import { CustomerEnquiryService } from '../../entities/customer-enquiry/customer-enquiry.service';

@QueryHandler(GetCustomerEnquiryQuery)
export class GetCustomerEnquiryQueryHandler implements IQueryHandler {
  constructor(private readonly enquiryService: CustomerEnquiryService) {}
  async execute() {
    return await this.enquiryService.findAll();
  }
}
