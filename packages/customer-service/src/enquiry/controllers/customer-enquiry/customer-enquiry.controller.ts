import {
  Controller,
  Post,
  Body,
  Get,
  UseGuards,
  ValidationPipe,
  UsePipes,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateNewCustomerEnquiryCommand } from '../../commands/create-new-customer-enquiry/create-new-customer-enquiry.command';
import { GetCustomerEnquiryQuery } from '../../queries/customer-enquiry/customer-enquiry.query';
import { TokenGuard } from '../../../guards/token.guard';
import { EnquiryDTO } from '../../entities/customer-enquiry/customer-enquiry-dto';
import { RoleGuard } from '../../../guards/role.guard';
import { Roles } from '../../../decorators/roles.decorator';
import { CUSTOMER_RELATION_MANAGER } from '../../../constants/app-string';

@Controller('api/enquiry')
export class CustomerEnquiryController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(ValidationPipe)
  async createNewEnquiry(@Body() payload: EnquiryDTO) {
    return await this.commandBus.execute(
      new CreateNewCustomerEnquiryCommand(payload),
    );
  }

  @Get('v1/list')
  @UsePipes(ValidationPipe)
  @Roles(CUSTOMER_RELATION_MANAGER)
  @UseGuards(TokenGuard, RoleGuard)
  async listEnquiry() {
    return await this.queryBus.execute(new GetCustomerEnquiryQuery());
  }
}
