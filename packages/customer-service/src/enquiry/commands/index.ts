import { CreateNewCustomerEnquiryHandler } from './create-new-customer-enquiry/create-new-customer-enquiry.handler';

export const EnquiryCommandHandlers = [CreateNewCustomerEnquiryHandler];
