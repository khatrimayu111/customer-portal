import { ICommand } from '@nestjs/cqrs';
import { EnquiryDTO } from '../../entities/customer-enquiry/customer-enquiry-dto';

export class CreateNewCustomerEnquiryCommand implements ICommand {
  constructor(public readonly payload: EnquiryDTO) {}
}
