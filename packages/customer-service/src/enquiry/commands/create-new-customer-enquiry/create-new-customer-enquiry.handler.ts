import { ICommandHandler, CommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateNewCustomerEnquiryCommand } from './create-new-customer-enquiry.command';
import { CustomerEnquiryAggregateManager } from '../../aggregates/customer-enquiry-aggregate-manager/customer-enquiry-aggregate-manager';

@CommandHandler(CreateNewCustomerEnquiryCommand)
export class CreateNewCustomerEnquiryHandler
  implements ICommandHandler<CreateNewCustomerEnquiryCommand> {
  constructor(
    private readonly manager: CustomerEnquiryAggregateManager,
    private readonly publisher: EventPublisher,
  ) {}
  async execute(command: CreateNewCustomerEnquiryCommand) {
    const { payload } = command;

    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.createNewCustomerInquiry(payload);
    aggregate.commit();
  }
}
