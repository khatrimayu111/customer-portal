import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { SetupService } from './setup.service';
import { SettingsDto } from '../../models/settings/settings.dto';
import { CUSTOMER_SERVICE } from '../../constants/app-string';

@Controller('setup')
export class SetupController {
  constructor(private readonly setupService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() payload: SettingsDto) {
    const settings = Object.assign({}, payload);
    settings.type = CUSTOMER_SERVICE;
    return await this.setupService.setup(payload);
  }
}
