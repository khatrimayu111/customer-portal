import { Injectable, HttpService } from '@nestjs/common';
import { Settings } from '../../models/settings/settings.collection';
import { SettingsService } from '../../models/settings/settings.service';
import { settingsAlreadyExists } from '../../constants/exceptions';
import { SOMETHING_WENT_WRONG } from '../../constants/messages';

@Injectable()
export class SetupService {
  protected Settings: Settings;

  constructor(
    protected readonly settingsService: SettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    if (await this.settingsService.count()) {
      throw settingsAlreadyExists;
    }

    this.http
      .get(params.authServerURL + '/.well-known/openid-configuration')
      .subscribe({
        next: async response => {
          params.authorizationURL = response.data.authorization_endpoint;
          params.tokenURL = response.data.token_endpoint;
          params.profileURL = response.data.userinfo_endpoint;
          params.revocationURL = response.data.revocation_endpoint;
          params.introspectionURL = response.data.introspection_endpoint;
          params.callbackURLs = [
            params.appURL + '/index.html',
            params.appURL + '/silent-refresh.html',
          ];
          this.Settings = await this.settingsService.save(params);
          return this.Settings;
        },
        error: error => {
          // TODO : meaningful errors
          throw SOMETHING_WENT_WRONG;
        },
      });
  }

  async getInfo() {
    const info = await this.settingsService.find();
    if (info) {
      delete info.clientSecret, info._id;
    }
    return info;
  }
}
